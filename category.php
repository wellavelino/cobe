<?php get_header() ?>

	<div class="acesso-pagina">
       <h3 id="acesso-pagina-referencia">TRABALHOS</h3>
      </div>
      <article>    

		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<?php $categories = get_the_category(); ?>

            <section class="category-<?php echo $categories[0]->name ?>">
                  <div class="container">
                      <div class="column-left">
                        <?php the_post_thumbnail(); ?>
                      </div>
                      <div class="column-right">
                      
                        <span class="icon"></span>
                        <h1><?php the_title() ?></h1> 
                        <h3><?php echo get_field('subtitulo') ?></h3>

                        <div class="conteudo">
                        <p><?php the_content() ?></p>
                        <div class="slider_wrapper">
                       
                           <ul class="bjqs">
                        
                        <?php $videos = get_field('video');
                          

                          if( $videos ): ?>
                            
                              <?php foreach( $videos as $video):  ?>
                                 <li>
                                   <iframe style="background: #fff" width="420" height="315" src="//www.youtube.com/embed/<?php echo get_field('id', $video); ?>" frameborder="0" allowfullscreen></iframe>
                                 </li>
                                 <?php endforeach ?>   

                            </ul> 
                               <?php endif  ?>                     
                                      
                       </div>                            
                           
                     </div>
                       <button  class="visualizarTrabalhos">
						 <span class="setaBaixo" ></span> 
                       Clique para ver todos os trabalhos </button>              
                      </div>
                                       
                  </div>
            </section>  
            <hr />
                           
		<?php endwhile ?>
	<?php endif ?>
   </article>

<?php get_footer() ?>	