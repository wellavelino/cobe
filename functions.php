<?php  function cobe_setup() {

    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 324 , 210, true );

}
add_action( 'after_setup_theme', 'cobe_setup' ); ?>