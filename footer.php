<footer id="rodape">     
    <div class="footer">
        <div class="container">
            <div class="endereco">
                <ul>
                    <h4>Endereço</h4>
                    <li>Rua: fernando de trejo, 421</li>                                     
                </ul>
             </div>              
             <div class="trabalheconosco">
                    <ul>
                        <h4>Trabalhe conosco</h4>
                        <li><a class="mail" href="mailto:contato@studiocobe.com.br">contato@studiocobe.com.br</a></li>
                    </ul>
             </div> 
             <div class="social-link">
                 <ul>
                     <h4>Redes Sociais</h4>
                      <li><a class="facebook sprite-social" href=""></a></li>
                      <li><a class="twitter sprite-social" href=""></a></li>
                      <li><a class="instagram   sprite-social" href=""></a></li>                   
                 </ul>
             </div>
            <!--  <div id="fb-root">
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>       
             </div>
             <div class="fb-like-box" data-href="https://www.facebook.com/facebook" data-colorscheme="light" 
                    data-show-faces="true" data-header="false" data-stream="false" data-show-border="true">
            </div> -->
             <div class="telefone">
                 <ul>
                     <h4>Telefone</h4>
                     <li>
                         (11) 99988-7656
                     </li>
                 </ul>
             </div>
        </div>
    </div>
</footer>
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.ba-hashchange.min.js"></script>                   
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.11.0.min.js"></script>           
        <script src="<?php echo get_template_directory_uri(); ?>/js/bjqs-1.3.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/menu.js"></script>        
    </body>
</html>