<?php get_header() ?>
      <div class="acesso-pagina">
                  <h3 id="acesso-pagina-referencia">TRABALHOS</h3>
      </div>
      <article>
             <?php
                  $query = new WP_Query(array('post_type' => 'trabalho'));
                  while ( $query->have_posts() ) :
                    $query->the_post();
                   $categories = wp_get_post_categories(get_the_ID());
                   $categories = get_category($categories[0]);
                ?>
         
            <section class="category-<?php echo $categories->name ?>">

                  <div class="container">
                      <div class="column-left">
                        <?php the_post_thumbnail(); ?>
                      </div>
                      <div class="column-right">
                      
                        <span class="icon"></span>
                        <h1><?php the_title() ?></h1> 
                        <h3><?php echo get_field('subtitulo') ?></h3>

                        
                        <div class="conteudo">                         
                        <p><?php the_content() ?></p>                                                
                        <div class="slider_wrapper">
                       
                           <ul class="bjqs">
                        
                        <?php $posts = get_field('video');
                          

                          if( $posts ): ?>
                            
                              <?php foreach( $posts as $post):  ?>
                               <?php setup_postdata($post); ?>
                                 <li>
                                   <iframe style="background: #fff" width="420" height="315" src="//www.youtube.com/embed/<?php echo get_field('id') ?>" frameborder="0" allowfullscreen></iframe>
                                 </li>   
                              <?php endforeach; ?>
                             <?php wp_reset_postdata();  ?>
                            </ul> 
                               <?php endif; ?>    
                       </div>
                      </div>
                        <button  class="visualizarTrabalhos">
                            <span class="setaBaixo" ></span> 
                                Clique para ver todos os trabalhos 
                        </button>              
                      </div>
                                       
                  </div>
            </section> 
               <hr />
              
            <?php endwhile;               
            ?>
      </article>
<?php get_footer() ?>
 

