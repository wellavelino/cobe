<!DOCTYPE html>
<html class="no-js">
    <link rel="img/icon/favicon.ico" type="image/ico" /><title>Studio Cobé</title> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css"/>
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>

        <div id="ciculador">
    <header id="cabecalho">
            <div class="">
               <h1 class="cobe"><a class="ir" href="">cobe</a></h1>
            <ul class="menu-cobe">

                  <li class="sobre-cobe"><a class="redireciona" href="<?php echo  get_permalink(5) ?>">Sobre</a></li> 
                  <li class="sobre-cobe"> | </li>  
                  <li class="sobre-cobe"><a class="redireciona" href="<?php echo esc_url( home_url( '/' ) ); ?>">Trabalhos</a></li>
            </ul>            
            <div class="cabecalhoLink">
            <nav class="quicksandbold">
                <div class="content-menu">
                     <span class="pesquisar">Pesquisar por :</span>
                          <ul class="menu">                           
                            
                              <li class="tooltip" title="Todos"><a  class="sprite sprite-todos" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a></li> 
                                                   
                              <li class="tooltip" title="trilha"><a  class="sprite sprite-trilha" href="<?php echo get_category_link( 4 ); ?> "></a></li>

                              <li class="tooltip" title="master"><a class="sprite sprite-master" href=" <?php echo get_category_link(3); ?>  "></a></li>

                              <li class="tooltip" title="mix"> <a class="sprite sprite-mix" href="<?php echo get_category_link( 2 ); ?> "></a></li> 

                              <li class="tooltip" title="Som Direto"> <a class="sprite sprite-somDireto" href="<?php echo get_category_link( 5 ); ?>   "></a></li>  
                          </ul>                
                </div>      
                <div class="content-menu">                
                     <span class="contato">contato@cobe.com.br &nbsp; (11) 99988-7656</span>
                         <ul class="social-menu">
                              <li ><a class="sprite sprite-facebook" href=""></a></li>
                              <li ><a class="sprite sprite-twitter" href=""></a></li>
                        </ul>
                </div>
            </nav>            
            </div>
            </div>
    </header>